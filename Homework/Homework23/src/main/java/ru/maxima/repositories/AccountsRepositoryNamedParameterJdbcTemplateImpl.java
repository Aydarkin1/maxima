package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.maxima.models.Account;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class AccountsRepositoryNamedParameterJdbcTemplateImpl implements AccountsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into account(email, password) values (:email, :password) RETURNING id";

    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from account order by id";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final RowMapper<Account> accountRowMapper = (row, rowNumber) -> Account.builder()
            .id(row.getLong("id"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();

    @Autowired
    public AccountsRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Account account) {
        // данный объект запоминает сгенерированные базой данных ключи
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, (new MapSqlParameterSource()
                .addValue("email", account.getEmail())
                .addValue("password", account.getPassword())), keyHolder, new String[]{"id"});
        account.setId(keyHolder.getKey().longValue());
    }

    @Override
    public List<Account> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }
}
