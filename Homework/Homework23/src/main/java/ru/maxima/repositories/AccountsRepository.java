package ru.maxima.repositories;

import ru.maxima.models.Account;

import java.util.List;

public interface AccountsRepository {
    void save(Account account);
    List<Account> findAll();
}
