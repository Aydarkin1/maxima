package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.config.ApplicationConfig;
import ru.maxima.services.UsersService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {

    private UsersService usersService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();

        writer.println("<h1>Email</h1>");
        writer.println("<table>");
        writer.println("    <tr>");
        writer.println("        <th>" + "Email of user" + "</th>");
        writer.println("    </tr>");
        for (String email : usersService.getAllEmails()) {
            writer.println("<tr>");
            writer.println("    <td>" + email + "</td>");
            writer.println("</tr>");
        }
        writer.println("</table>");
    }
}
