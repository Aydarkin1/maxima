package ru.maxima.services;

import java.util.List;

public interface UsersService {
    void signUp(String email, String password);

    List<String> getAllEmails();
}
