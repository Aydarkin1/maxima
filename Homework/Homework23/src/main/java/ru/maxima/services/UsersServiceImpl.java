package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public UsersServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public void signUp(String email, String password) {
        Account account = Account.builder()
                .email(email)
                .password(password)
                .build();

        accountsRepository.save(account);
    }

    @Override
    public List<String> getAllEmails() {
        return accountsRepository.findAll().stream().map(Account::getEmail).collect(Collectors.toList());
    }
}
