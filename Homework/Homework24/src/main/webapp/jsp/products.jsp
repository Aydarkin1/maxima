<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script>
    function addProducts(id, Name) {
        let body = {
            "id": id,
            "Name": Name
        };

        let request = new XMLHttpRequest();

        request.open('POST', '/products', false);
        request.setRequestHeader("Content-Type", "application/json");
        request.send(JSON.stringify(body));

        if (request.status !== 201) {
            alert("Ошибка!")
        }

    }
</script>
<body>
<h1 style="color: ${color}">Products page</h1>
<div>
    <label for="id">ID:</label>
    <input id="id" name="id" placeholder="Example: 45"/>
    <label for="Name">Name:</label>
    <input id="Name" name="Name" placeholder="Example: Молоко">
    <button onclick="addProducts(
        document.getElementById('id').value,
        document.getElementById('Name').value)">Add
    </button>
</div>
</body>
</html>
