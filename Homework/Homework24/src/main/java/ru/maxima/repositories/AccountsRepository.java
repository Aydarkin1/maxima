package ru.maxima.repositories;

import ru.maxima.models.Account;

import java.util.List;
import java.util.Optional;


public interface AccountsRepository {
    void save(Account account);
    List<Account> findAll();
    Optional<Account> findByEmail(String email);

    List<Account> searchByEmail(String email);
}
