package ru.maxima.servlets;

import ru.maxima.dto.AccountDto;
import org.springframework.context.ApplicationContext;
import ru.maxima.exceptions.IncorrectEmailOrPasswordException;
import ru.maxima.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet("/users")
public class UsersServlet extends HttpServlet {

    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.usersService = applicationContext.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AccountDto> accounts = usersService.getAllUsers();
        request.setAttribute("accounts", accounts);
        request.getRequestDispatcher("/jsp/users.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
          try {
        usersService.signIn(email, password);
            HttpSession session = request.getSession(true);
            session.setAttribute("isAuthenticated", true);
            response.sendRedirect("/profile");
        }catch (IncorrectEmailOrPasswordException e) {
              response.sendRedirect("/signIn");

        }
    }
}
