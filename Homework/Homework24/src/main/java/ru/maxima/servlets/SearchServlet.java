package ru.maxima.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.maxima.dto.AccountDto;
import ru.maxima.dto.AccountsResponseDto;
import ru.maxima.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/searchByUsers")
public class SearchServlet extends HttpServlet {

    private UsersService usersService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.usersService = applicationContext.getBean(UsersService.class);
        this.objectMapper = applicationContext.getBean(ObjectMapper.class);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("email") == null) {
            request.getRequestDispatcher("/jsp/search_page.jsp").forward(request, response);
        } else {
            List<AccountDto> accounts = usersService.searchUserByEmail(request.getParameter("email"));
            String jsonResponse = objectMapper.writeValueAsString(new AccountsResponseDto(accounts));
            response.setStatus(200);
            response.setContentType("application/json");
            response.getWriter().println(jsonResponse);
        }
    }
}
