package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.exceptions.IncorrectEmailOrPasswordException;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.List;
import java.util.stream.Collectors;

import static ru.maxima.dto.AccountDto.from;


@Service
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public UsersServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public void signUp(String email, String password) {
        Account account = Account.builder()
                .email(email)
                .password(password)
                .build();

        accountsRepository.save(account);
    }

    @Override
    public void signIn(String email, String password) throws IncorrectEmailOrPasswordException {
   Account account = accountsRepository.findByEmail(email).orElseThrow(IncorrectEmailOrPasswordException::new);
        if(!account.getPassword().equals(password)){
            throw new IncorrectEmailOrPasswordException();
        }
    }

    @Override
    public List<String> getAllEmails() {
        return null;
    }


    @Override
    public List<AccountDto> getAllUsers() {
        return from(accountsRepository.findAll());
    }

    @Override
    public List<AccountDto> searchUserByEmail(String email) {
        return from(accountsRepository.searchByEmail(email));
    }


}
