package ru.maxima.services;

import ru.maxima.dto.AccountDto;

import java.util.List;


public interface UsersService {
    void signUp(String email, String password);
    void signIn(String email,String password);
    List<String> getAllEmails();
    List<AccountDto> getAllUsers();

    List<AccountDto> searchUserByEmail(String email);
}
