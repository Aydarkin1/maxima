package ru.maxima.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    private static final String IS_AUTHENTICATED_ATTRIBUTE_NAME = "isAuthenticated";
    private static final List<String> PROTECTED_URLS = Arrays.asList("/users", "/searchByUsers", "/products", "/Profile");
    private static final List<String> NOT_ACCESSED_FOR_AUTHENTICATED_URLS = Arrays.asList(
            "/signIn",
            "/signUp"
    );
    private static final String DEFAULT_REDIRECT_URL = "/profile";
    private static final String DEFAULT_SIGN_IN_URL = "/signIn";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (isProtected(request.getRequestURI())) {
            if (isAuthenticated(request)) {
                if (isNotAccessedForAuthenticated(request.getRequestURI())) {
                    response.sendRedirect(DEFAULT_REDIRECT_URL);
                    return;
                    chain.doFilter(request, response);
                } else {
                    response.sendRedirect(DEFAULT_SIGN_IN_URL);
                    return;
                }
            }
            chain.doFilter(request, response);
        }

        private boolean isProtected (String url){
            return PROTECTED_URLS.contains(url);
        }
        private boolean isNotAccessedForAuthenticated (String url){
            return NOT_ACCESSED_FOR_AUTHENTICATED_URLS.contains(url);
        }
        private boolean isAuthenticated (HttpServletRequest request){
            HttpSession session = request.getSession(false);
            if (session == null) {
                return false;
            }
            Boolean result = (Boolean) session.getAttribute(IS_AUTHENTICATED_ATTRIBUTE_NAME);
            return result != null && result;
        }

        @Override
        public void destroy () {

        }
    }
}


