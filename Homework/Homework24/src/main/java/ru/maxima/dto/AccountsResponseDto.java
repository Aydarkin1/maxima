package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@Data
@AllArgsConstructor
public class AccountsResponseDto {
    private List<AccountDto> accounts;
}
